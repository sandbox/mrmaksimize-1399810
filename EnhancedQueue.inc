<?php

interface DrupalEnhancedQueueInterface extends DrupalQueueInterface{

  /**
   * Add a queue item and store it directly to the queue.
   *
   * @param $data
   *   Arbitrary data to be associated with the new task in the queue.
   * @param $q_key
   *   A unique identifier of the queue key that can be used to retrieve items from the queue later
   * @param $q_item_id
   *   A unique identifier of the item id, so that if needed the queue can be deduped
   * @return
   *   TRUE if the item was successfully created and was (best effort) added
   *   to the queue, otherwise FALSE. We don't guarantee the item was
   *   committed to disk etc, but as far as we know, the item is now in the
   *   queue.
   */
  public function createKeyedItem($data, $q_key = NULL, $q_item_id = NULL);

  /**
   * Retrieve the number of items in the queue.
   *
   * This is intended to provide a "best guess" count of the number of items in
   * the queue. Depending on the implementation and the setup, the accuracy of
   * the results of this function may vary.
   *
   * e.g. On a busy system with a large number of consumers and items, the
   * result might only be valid for a fraction of a second and not provide an
   * accurate representation.
   * @param $q_key
   *   A unique identifier of the queue key that can be used to retrieve items from the queue
   * @return
   *   An integer estimate of the number of items in the queue.
   */
  public function numberOfItemsWithKey($q_key);

  /**
   * Retrieve number of items with a specific id from the queue,  If optional q_key is passed,
   * the search will be filtered by that key as well
   */
  public function numberOfItemsWithId($q_item_id, $q_key = NULL);

  /**
   * A helper function that looks through the queue, finds duplicates and deletes them
   **/
  public function dedupItem($q_item_id, $q_key = NULL);
  /**
   * Claim an item in the queue for processing.
   *
   * @param $lease_time
   *   How long the processing is expected to take in seconds, defaults to an
   *   hour. After this lease expires, the item will be reset and another
   *   consumer can claim the item. For idempotent tasks (which can be run
   *   multiple times without side effects), shorter lease times would result
   *   in lower latency in case a consumer fails. For tasks that should not be
   *   run more than once (non-idempotent), a larger lease time will make it
   *   more rare for a given task to run multiple times in cases of failure,
   *   at the cost of higher latency.
   * @param $q_key
   *   A unique identifier of the queue key that can be used to retrieve items from the queue
   * @param $q_item_id
   *   An item it.  Passing both this and q_key will cause filtering to work within the q_key
   * @return
   *   On success we return an item object. If the queue is unable to claim an
   *   item it returns false. This implies a best effort to retrieve an item
   *   and either the queue is empty or there is some other non-recoverable
   *   problem.
   */

  public function claimKeyedItem($lease_time = 3600, $q_key = NULL, $q_item_id = NULL);

  /**
   * Function that updates an item in queue, and / or releases
   * this function is a workaround for releaseItem which expires the item.  When you have a loop
   * this will cause an endless loop if the item is expired.
   * It also has the capability of updating the item before releasing it
   *
   * @param $item
   *  Item return from claimItem or claimKeyedItem
   * @param $expire
   *  Should the item be expired
   *  (this equivalent to calling releaseItem, but with the ability to change data)
   * @return boolean
   */

  public function updateItemInQueue($item, $expire = TRUE);

  /**
   * When working with keyed queues, a lot of times in a loop you may need to expire an item,
   * But doing so would cause an endless loop;  However, this function acts as cleanup.  You can call
   * updateItemInQueue to modify the items and NOT expire them, and after your loop call this to update
   * all the items you worked on (in a given key) to be expired.
   *
   * @param $q_key
   *  Unique identifier of the Queue key
   * @return boolean
   */
  public function expireItemsForKey($q_key);

  /**
   * Mass delete items either within a q_key or a q_item_id.
   */
  public function deleteItemsForKey($q_key = NULL, $q_item_id = NULL, $expired_only = TRUE);

}

class EnhancedQueue extends SystemQueue implements DrupalEnhancedQueueInterface{
  /**
   * Create and item in the queue
   */
  public function createKeyedItem($data, $q_key = NULL, $q_item_id = NULL) {
    if ($q_key == NULL && $q_item_id == NULL){
      return parent::createItem($data);
    }
    else{
      $fields = array_filter(array(
        'name' => $this->name,
        'data' => serialize($data),
        'created' => time(),
        'q_key' => $q_key,
        'q_item_id' => $q_item_id,
      ));
    }
    $query = db_insert('queue')
      ->fields($fields);
    return (bool) $query->execute();
  }

  /**
   * Retrieve number of items within a q_key
   */
  public function numberOfItemsWithKey($q_key = NULL) {
    //watchdog('queue', 'Current item count = %num', array('%num' => $count));
    if ($q_key == NULL){
      return parent::numberOfItems();
    }
    else{
      return db_query('SELECT COUNT(item_id) FROM {queue} WHERE name = :name AND q_key = :q_key', array(':name' => $this->name, ':q_key' => $q_key))->fetchField();
    }
  }

  /**
  * Retrieve number of items with a specific id from the queue,  If optional q_key is passed,
  * the search will be filtered by that key as well
  */
  public function numberOfItemsWithId($q_item_id, $q_key = NULL) {
    //fall back to parent
    if ($q_key == NULL && $q_item_id == NULL){
      return parent::numberOfItems();
    }
    else{
      if ($q_key){
        return db_query('SELECT COUNT(item_id) FROM {queue} WHERE name = :name AND q_key = :q_key AND q_item_id = :q_item_id', array(':name' => $this->name, ':q_key' => $q_key, ':q_item_id' => $q_item_id))->fetchField();
      }
      else{
        return db_query('SELECT COUNT(item_id) FROM {queue} WHERE name = :name AND q_item_id = :q_item_id', array(':name' => $this->name, ':q_item_id' => $q_item_id))->fetchField();
      }
    }
  }


   /**
   * A helper function that looks through the queue, finds duplicates and deletes them
   * if q_key is passed, items are deduped only within that q_key
   **/
  public function dedupItem($q_item_id, $q_key = NULL){
    if ($this->numberOfItemsWithId($q_item_id, $q_key) > 1){
      $items = array();
      while($item = $this->claimKeyedItem(3600, $q_key, $q_item_id)){
        $items[] = $item;
      }
      //since there is an order sort on the claim, we can be sure that the last item in the array is newest
      $current_item = array_pop(&$items);
      foreach ($items as $item){
        $this->deleteItem($item);
      }
      $this->releaseItem($current_item);
    }
    else{
      return TRUE;
    }
  }
  /**
  * Claim an item in the queue for processing.
  */
  public function claimKeyedItem($lease_time = 3600, $q_key = NULL, $q_item_id = NULL) {
    while (TRUE) {
      if ($q_key == NULL && $q_item_id == NULL){
        return parent::claimItem($lease_time);
      }
      else{
        //this op is for normal pulling from queue
        if ($q_item_id == NULL && $q_key != NULL){
          $item = db_query_range('SELECT data, item_id FROM {queue} q WHERE expire = 0 AND name = :name AND q_key = :q_key ORDER BY created ASC', 0, 1, array(':name' => $this->name, ':q_key' => $q_key))->fetchObject();
        }
        //retrieve an item with an id from a q_key
        else if ($q_item_id != NULL && $q_key != NULL){
          $item = db_query_range('SELECT data, item_id FROM {queue} q WHERE expire = 0 AND name = :name AND q_key = :q_key AND q_item_id = :q_item_id ORDER BY created ASC', 0, 1, array(':name' => $this->name, ':q_key' => $q_key, ':q_item_id' => $q_item_id))->fetchObject();
        }
        //retrieve an item with an id from the entire queue
        else if ($q_item_id != NULL && $q_key == NULL){
          $item = db_query_range('SELECT data, item_id FROM {queue} q WHERE expire = 0 AND name = :name AND q_item_id = :q_item_id ORDER BY created ASC', 0, 1, array(':name' => $this->name, ':q_item_id' => $q_item_id))->fetchObject();
        }
      }
      if ($item) {
        $update = db_update('queue')
          ->fields(array(
            'expire' => time() + $lease_time,
          ))
          ->condition('item_id', $item->item_id)
          ->condition('expire', 0);
          // If there are affected rows, this update succeeded.
        if ($update->execute()) {
          $item->data = unserialize($item->data);
          $item->data->q_key = $q_key;
          $item->data->q_item_id = $q_item_id;
          return $item;
        }
      }
      else {
        // No items currently available to claim.
        return FALSE;
      }
    }
  }
  /**
   * Function that updates an item in queue, and / or releases
   */
  public function updateItemInQueue($item, $expire = TRUE) {
    $fields = array();
    if ($expire == TRUE){
      $fields['expire'] = 0;
    }
    $fields['data'] = serialize($item->data);
    $update = db_update('queue')
      ->fields($fields)
      ->condition('item_id', $item->item_id);
    return $update->execute();
  }

  public function expireItemsForKey($q_key){
    $update = db_update('queue')
      ->fields(array('expire' => 0))
      ->condition('expire', 0, '>')
      ->condition('q_key', $q_key);
    return $update->execute();
  }
  /**
   * Mass delete items either within a q_key or a q_item_id.
   */
  public function deleteItemsForKey($q_key = NULL, $q_item_id = NULL, $expired_only = TRUE){
    $delete = db_delete('queue');
    if ($q_key != NULL){
      $delete->condition('q_key', $q_key);
    }
    if ($q_item_id != NULL){
      $delete->condition('q_item_id', $q_item_id);
    }
    if ($expired_only == TRUE){
      $delete->condition('expire', 0);
    }
    $return =  $delete->execute();
    return $return;
  }
}
